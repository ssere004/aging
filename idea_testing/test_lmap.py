import pandas as pd
import configs
from scipy.stats import hypergeom

df = pd.read_csv('/home/ssere004/projects/aging/data/humanmethylation450_15017482_v1-2.csv', sep=',', skiprows=7)
df = df[['Name', 'CHR', 'MAPINFO']]
cpg_list = pd.read_csv('/home/ssere004/projects/aging/data/rapid_gain_cpgs.txt', header=None, names=['Name'])
#cpg_list = pd.read_csv('/home/ssere004/projects/aging/data/353_cpg.txt', header=None, names=['Name'])
df = cpg_list.merge(df, left_on=['Name'], right_on=['Name'], how='inner')
df.columns = ['Name', 'chr', 'position']
df['position'] = df['position'].astype(int)
df['chr'] = df['chr'].astype(int)
df['chr'] = df['chr'].apply(lambda x: 'chr' + str(x))

#test weather datasets are referring to cytosines
for index, row in df.iterrows():
    if assembly[row['chr']][row['position']-1] != 'C':
        print(index)
        break

import numpy as np
import random
num_random_Cs = 5000
chrs = list(assembly.keys())
chr_sizes = [len(assembly[chro]) for chro in chrs]
chr_list = random.choices(chrs, chr_sizes, k=num_random_Cs)
C_list = np.zeros(num_random_Cs, dtype=int)
for i in range(num_random_Cs):
    found_C = False
    while not found_C:
        c_idx = random.randint(0, len(assembly[chr_list[i]]))
        if assembly[chr_list[i]][c_idx] == 'C' and assembly[chr_list[i]][c_idx+1] == 'G':
            C_list[i] = c_idx
            found_C = True
rand_df = pd.DataFrame({'chr': chr_list, 'position': C_list})

dataset = dmngr.make_input_dataset(assembly, rand_df, 1, window_size=window_size)


X = dataset.drop(columns=['label'])
Y = np.asarray(pd.cut(dataset['label'], bins=2, labels=[0, 1], right=False))
b = np.zeros((Y.size, Y.max()+1))
b[np.arange(Y.size), Y] = 1
Y = b

batch_size = 32
labels = torch.tensor(Y, dtype=torch.float32)
dataset = DNADataset(X, labels, tokenizer, window_size, kmer)
data_loader_test = DataLoader(dataset, batch_size=batch_size, shuffle=True)

total_samples_test = len(data_loader_test.dataset)

clf_model.to('cuda')
clf_model.eval()
test_correct = 0
#keep track of the predicted and labels to draw the ROC curve
predicted_test, labels_test = np.zeros((total_samples_test, 2)) - 1, np.zeros((total_samples_test, 2)) - 1
idx = 0

with torch.no_grad():
    for batch in data_loader_test:
        input_ids = batch["input_ids"].to('cuda')
        attention_mask = batch["attention_mask"].to('cuda')
        target_labels = batch["label"].to('cuda')
        methylations = batch["methylations"].to('cuda')
        input_ids.requires_grad = False
        attention_mask.requires_grad = False
        target_labels.requires_grad = False
        logits, _ = clf_model(input_ids, attention_mask, methylations=methylations)
        predicted_labels = logits.argmax(dim=1)
        test_correct += (predicted_labels == target_labels.argmax(dim=1)).sum().item()
        predicted_test[idx: idx + len(logits)] = logits.cpu().numpy()
        labels_test[idx: idx + len(target_labels)] = target_labels.cpu().numpy()
        idx += len(logits)


binary_prediction = (predicted_test[:, 1] > predicted_test[:, 0]).astype(int)
