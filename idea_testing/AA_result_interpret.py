import pandas as pd
from scipy.stats import ttest_ind
from statsmodels.stats.multitest import multipletests
from pandarallel import pandarallel
import numpy as np


######################################################################
#make a sheet of Age Accelerations using different clocks and different cancers

cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']
clocks = ['Horvath', 'Hannum', 'Levine', 'BNN', 'skinHorvath', 'BLUP', 'EN']
cols = ['cohort_name']
for clock in clocks:
    cols.append(f'{clock}_Normal_AA')
    cols.append(f'{clock}_Tumor_AA')

res_df = pd.DataFrame({col: [] for col in cols})

for cancer in cancers:
    df = pd.read_csv(f'~/Desktop/Data/pyprojs/Aging/data_subsample/{cancer}_pred_chron_result.csv', sep='\t')
    row_dic = {'cohort_name': cancer}
    for clock in clocks:
        normal_aa = np.mean(df[df['tissue_type'] == 'Normal'][clock] - df[df['tissue_type'] == 'Normal']['c_age'])
        tumor_aa = np.mean(df[df['tissue_type'] == 'Tumor'][clock] - df[df['tissue_type'] == 'Tumor']['c_age'])
        row_dic[f'{clock}_Normal_AA'] = normal_aa
        row_dic[f'{clock}_Tumor_AA'] = tumor_aa
    res_df = pd.concat([res_df, pd.DataFrame([row_dic])], ignore_index=True)

res_df.to_csv('~/Desktop/Data/pyprojs/Aging/data_subsample/aa_results.csv', index=False, sep=',')



#################################################################


cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']
clocks = ['Horvath', 'Hannum', 'Levine', 'BNN', 'skinHorvath', 'BLUP', 'EN']
cols = ['clock_name']
for cancer in cancers:
    cols.append(f'{cancer}_Normal_AA')
    cols.append(f'{cancer}_Tumor_AA')

res_df = pd.DataFrame({col: [] for col in cols})

for clock in clocks:
    row_dic = {'clock_name': clock}
    for cancer in cancers:
        df = pd.read_csv(f'~/Desktop/Data/pyprojs/Aging/data_subsample/{cancer}_pred_chron_result.csv', sep='\t')
        normal_aa = np.mean(df[df['tissue_type'] == 'Normal'][clock] - df[df['tissue_type'] == 'Normal']['c_age'])
        tumor_aa = np.mean(df[df['tissue_type'] == 'Tumor'][clock] - df[df['tissue_type'] == 'Tumor']['c_age'])
        row_dic[f'{cancer}_Normal_AA'] = normal_aa
        row_dic[f'{cancer}_Tumor_AA'] = tumor_aa
    res_df = pd.concat([res_df, pd.DataFrame([row_dic])], ignore_index=True)

res_df.to_csv('~/Desktop/Data/pyprojs/Aging/data_subsample/aa_results_clock_rows.csv', index=False, sep=',')


##################################################################################################
cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']



def count_normal_tumor_samples(fn, alias):
    df = pd.read_csv(fn, sep='\t')
    return len(df[df.tissue_type == 'Normal']), len(df[df.tissue_type == 'Tumor']), alias


def rename_columns(df, c_df):
    c_df_dict = c_df.set_index('id').to_dict(orient='index')
    def create_new_col_name(col_name):
        if col_name == 'ProbeID':
            return col_name  # Keep 'ProbeID' unchanged
        if col_name in c_df_dict:
            tissue_type = c_df_dict[col_name]['tissue_type']
            c_age = c_df_dict[col_name]['c_age']
            # Format the c_age with two decimal places and create the new name
            new_col_name = f"{col_name}_{tissue_type[0]}_{c_age:.2f}"
            return new_col_name
        return col_name  # Return original name if no match found
    df.columns = [create_new_col_name(col) for col in df.columns]
    return df


def filter_lownn_cols(df):
    t_columns = df.columns[df.columns.str.contains('_T_')]
    n_columns = df.columns[df.columns.str.contains('_N_')]
    nan_percentage_t = df[t_columns].isna().mean(axis=1)
    nan_percentage_n = df[n_columns].isna().mean(axis=1)
    return df[(nan_percentage_t <= 0.30) & (nan_percentage_n <= 0.30)]

def calculate_p_value(row):
    t_values = row[t_columns].dropna()  # Drop NaNs from '_T_' group
    n_values = row[n_columns].dropna()  # Drop NaNs from '_N_' group
    if len(t_values) > 1 and len(n_values) > 1:  # Ensure there are enough data points
        t_stat, p_value = ttest_ind(t_values, n_values, equal_var=False)
        return p_value
    else:
        return 1  # Assign a high p-value if not enough data points

def mean_difference(row):
    t_mean = row[t_columns].mean()  # Mean of '_T_' columns
    n_mean = row[n_columns].mean()  # Mean of '_N_' columns
    return abs(t_mean - n_mean)  # Absolute difference


def proportional_difference(row):
    t_values = row[t_columns].dropna()  # Drop NaNs from '_T_' group
    n_values = row[n_columns].dropna()  # Drop NaNs from '_N_' group
    n_mean = n_values.mean()  # Mean of '_N_' group
    t_mean = t_values.mean()  # Mean of '_T_' group
    # If T group mean is greater than N group mean, find proportion of T values greater than N mean
    if t_mean > n_mean:
        proportion = (t_values > n_mean).mean()
    # If T group mean is smaller than N group mean, find proportion of T values smaller than N mean
    else:
        proportion = (t_values < n_mean).mean()
    return proportion

suffix = '_pred_chron_result.csv'
#cancer = 'BMnBLD'
for cancer in cancers:
    c_df = pd.read_csv(f'./{cancer}/{cancer}{suffix}', sep='\t')
    df = pd.read_csv(f'{cancer}_methyl_cohort.csv')
    df = rename_columns(df, c_df)
    df = filter_lownn_cols(df)
    pandarallel.initialize(progress_bar=True)
    t_columns = df.columns[df.columns.str.contains('_T_')]
    n_columns = df.columns[df.columns.str.contains('_N_')]
    df['p_value'] = df.parallel_apply(calculate_p_value, axis=1)
    _, fdr_values, _, _ = multipletests(df['p_value'], method='fdr_bh')
    df['fdr'] = fdr_values
    df['mean_diff'] = df.parallel_apply(mean_difference, axis=1)
    df['proportional_diff'] = df.parallel_apply(proportional_difference, axis=1)
    cgs = df[(df.fdr < 0.01) & (df.mean_diff > 0.1) & (df.proportional_diff > 0.8)]['ProbeID']
    np.save(f'./{cancer}/{cancer}_filtered_cgs.npy', np.asarray(cgs.values))

################################################

import numpy as np
cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']

suffix = "_filtered_cgs.npy"
Cs = []
for cancer in cancers:
    Cs.append(np.load(f'./{cancer}/{cancer}{suffix}', allow_pickle=True))

intersection = set(Cs[0]).intersection(*Cs[1:])


####################################################

import os

def find_age_associated_cpgs(df, c_df):
    df = rename_columns(df, c_df)
    df = filter_lownn_cols(df)
    df.index = df['ProbeID']
    n_columns = df.columns[df.columns.str.contains('_N_')]
    n_columns = [list(n_columns)[i] for i in range(len(n_columns))]
    ages = np.array([float(s.split('_')[-1]) for s in n_columns])
    pandarallel.initialize(progress_bar=True, nb_workers=int(os.cpu_count()/2))
    def row_correlation(row):
        mask = ~np.isnan(row) & ~np.isnan(ages)
        return np.corrcoef(row[mask], ages[mask])[0, 1]
    df = df[n_columns]
    df['correlation'] = df.parallel_apply(row_correlation, axis=1)
    df['max_min_diff'] = df.drop(columns=['correlation']).apply(lambda row: row.max() - row.min(), axis=1)
    df[['correlation', 'max_min_diff']].to_csv(f'./{cancer}/{cancer}_cpg_age_correlations.csv')

cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']
for cancer in cancers:
    suffix = '_pred_chron_result.csv'
    c_df = pd.read_csv(f'./{cancer}/{cancer}{suffix}', sep='\t')
    find_age_associated_cpgs(pd.read_csv(f'{cancer}_methyl_cohort.csv'), c_df)

#################################################################################

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']
fig, axes = plt.subplots(2, 4, figsize=(16, 12))  # 4 rows and 2 columns
axes = axes.flatten()  # Flatten the 2D array of axes for easier indexing

for i, cancer in enumerate(cancers):
    df = pd.read_csv(f'/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/age_corr_Nsamples/{cancer}_cpg_age_correlations.csv', index_col=0)
    cpgs = np.load(f'/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/NT_DMCs/{cancer}_filtered_cgs.npy', allow_pickle=True)
    cpgs_df = df.loc[cpgs]
    df = df[df['max_min_diff'] > 0.5]
    cpgs_df = cpgs_df[cpgs_df['max_min_diff'] > 0.5]
    data_to_plot = [df['correlation'].dropna(), cpgs_df['correlation'].dropna()]
    axes[i].violinplot(data_to_plot)
    axes[i].set_ylim([-0.8, 1.0])
    if cancer in ['Lung', 'BMnBLD']:
        axes[i].set_ylabel('mC level correlation with age', fontsize=14)
    else:
        axes[i].set_yticklabels([])  # Remove y-axis labels for other plots
    alias_dic = {'Lung': 'Lung', 'Clorectal': 'Colorectal', 'Breast': 'Breast', 'Thyroid': 'Thyroid', 'BMnBLD': 'Bone Marrow and Blood', 'Kidney': 'Kidney', 'uterus': 'Uterus', 'HeadnNeck': 'Head and Neck'}
    axes[i].set_title(f'{alias_dic[cancer]}', fontsize=14)
    axes[i].axhline(y=0, color='red', linestyle='--')
    axes[i].text(0.1, 0.025, f"n={len(df):,}", transform=axes[i].transAxes,
                 fontsize=14, verticalalignment='bottom', horizontalalignment='left',
                 bbox=dict(facecolor='white', alpha=0.5))
    axes[i].text(0.90, 0.025, f"n={len(cpgs_df):,}", transform=axes[i].transAxes,
                 fontsize=14, verticalalignment='bottom', horizontalalignment='right',
                 bbox=dict(facecolor='white', alpha=0.5))
    if i >= 4:  # Index for the second row
        axes[i].set_xticks([1, 2])
        axes[i].set_xticklabels(['All CpGs', 'Cancer Affected \n CpGs'], fontsize=14)
    else:
        axes[i].set_xticks([])

plt.subplots_adjust(hspace=5)
plt.tight_layout()
plt.savefig('/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/Result/correlation_dist.png')

####################################################################################################


import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

clocks = ['Horvath', 'Hannum', 'Levine', 'BNN', 'skinHorvath', 'BLUP', 'EN']
clock_name = clocks[0]

# for clock_name in clocks
cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']
fig, axes = plt.subplots(7, 8, figsize=(28, 20))  # 4 rows and 2 columns
axes = axes.flatten()  # Flatten the 2D array of axes for easier indexing
is_subsample = False
subsample_alias = "_subsample" if is_subsample else ""
axi=0
for clock_name in clocks:
    mins, maxs = [], []
    for i, cancer in enumerate(cancers):
        df = pd.read_csv(f'~/Desktop/Data/pyprojs/Aging/data{subsample_alias}/{cancer}_pred_chron_result.csv', sep='\t')
        df['age_diff'] = df[clock_name] - df['c_age']
        df_normal = df[df['tissue_type'] == 'Normal']
        df_tumor = df[df['tissue_type'] == 'Tumor']
        mins.append(min(df_normal['age_diff'].min(), df_tumor['age_diff'].min()))
        maxs.append(max(df_normal['age_diff'].max(), df_tumor['age_diff'].max()))
    global_min = min(mins)
    global_max = max(maxs)
    xticks = np.arange(np.ceil(global_min / 50) * 50, np.ceil(global_max / 50) * 50, 50)
    for i, cancer in enumerate(cancers):
        df = pd.read_csv(f'~/Desktop/Data/pyprojs/Aging/data{subsample_alias}/{cancer}_pred_chron_result.csv', sep='\t')
        df['age_diff'] = df[clock_name] - df['c_age']
        df_normal = df[df['tissue_type'] == 'Normal']
        df_tumor = df[df['tissue_type'] == 'Tumor']
        sns.kdeplot(df_normal['age_diff'], label='Normal', fill=True, color='blue', ax=axes[axi])
        sns.kdeplot(df_tumor['age_diff'], label='Tumor', fill=True, color='red', ax=axes[axi])
        alias_dic = {'Lung': 'Lung', 'Clorectal': 'Colorectal', 'Breast': 'Breast', 'Thyroid': 'Thyroid', 'BMnBLD': 'Bone Marrow and Blood', 'Kidney': 'Kidney', 'uterus': 'Uterus', 'HeadnNeck': 'Head and Neck'}
        if axi < 8:
            axes[axi].set_title(f'{alias_dic[cancer]}', fontsize=16)
        else:
            axes[axi].set_title(f'', fontsize=0)
        axes[axi].set_ylim([0, 0.08])
        axes[axi].set_xlim([-100, 200])
        # axes[axi].set_xticks(xticks)
        axes[axi].tick_params(axis='x', labelsize=10)
        if axi >= 48:  # Index for the second row
            axes[axi].set_xlabel('Age Acceleration', fontsize=16)
        else:
            axes[axi].set_xlabel('')
        axes[axi].tick_params(axis='x', labelsize=16)
        if axi % 8 == 0:
            axes[axi].set_ylabel('Density', fontsize=16)
        else:
            axes[axi].set_ylabel('', fontsize=0)
            axes[axi].set_yticklabels([])
        axes[axi].axvline(x=0, color='red', linestyle='--')
        axi+=1

handles, labels = axes[0].get_legend_handles_labels()
fig.legend(handles, ['Normal', 'Tumor'], loc='upper center', fontsize=18, ncol=2, bbox_to_anchor=(0.5, 0.03))
plt.tight_layout()
plt.subplots_adjust(hspace=1, bottom=0.08)  # Increase bottom padding for the legend
plt.gcf().set_size_inches(28, 20)
plt.savefig(f'/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/Result/plots/AA_dist/AA_distributions_{subsample_alias}.png')



#########################################################################################
import pandas as pd
from scipy.stats import ttest_ind
from statsmodels.stats.multitest import multipletests
from pandarallel import pandarallel
import numpy as np


######################################################################
#make a sheet of Age Accelerations using different clocks and different cancers

cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']
clocks = ['Horvath', 'Hannum', 'Levine', 'BNN', 'skinHorvath', 'BLUP', 'EN']
cols = ['cohort_name']
for clock in clocks:
    cols.append(f'{clock}_Normal_AA')
    cols.append(f'{clock}_Tumor_AA')

res_df = pd.DataFrame({col: [] for col in cols})

for cancer in cancers:
    df = pd.read_csv(f'~/Desktop/Data/pyprojs/Aging/data_subsample/{cancer}_pred_chron_result.csv', sep='\t')
    row_dic = {'cohort_name': cancer}
    for clock in clocks:
        normal_aa = np.mean(df[df['tissue_type'] == 'Normal'][clock] - df[df['tissue_type'] == 'Normal']['c_age'])
        tumor_aa = np.mean(df[df['tissue_type'] == 'Tumor'][clock] - df[df['tissue_type'] == 'Tumor']['c_age'])
        row_dic[f'{clock}_Normal_AA'] = normal_aa
        row_dic[f'{clock}_Tumor_AA'] = tumor_aa
    res_df = pd.concat([res_df, pd.DataFrame([row_dic])], ignore_index=True)

res_df.to_csv('~/Desktop/Data/pyprojs/Aging/data_subsample/aa_results.csv', index=False, sep=',')



#################################################################


cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']
clocks = ['Horvath', 'Hannum', 'Levine', 'BNN', 'skinHorvath', 'BLUP', 'EN']
cols = ['clock_name']
for cancer in cancers:
    cols.append(f'{cancer}_Normal_AA')
    cols.append(f'{cancer}_Tumor_AA')

res_df = pd.DataFrame({col: [] for col in cols})

for clock in clocks:
    row_dic = {'clock_name': clock}
    for cancer in cancers:
        df = pd.read_csv(f'~/Desktop/Data/pyprojs/Aging/data_subsample/{cancer}_pred_chron_result.csv', sep='\t')
        normal_aa = np.mean(df[df['tissue_type'] == 'Normal'][clock] - df[df['tissue_type'] == 'Normal']['c_age'])
        tumor_aa = np.mean(df[df['tissue_type'] == 'Tumor'][clock] - df[df['tissue_type'] == 'Tumor']['c_age'])
        row_dic[f'{cancer}_Normal_AA'] = normal_aa
        row_dic[f'{cancer}_Tumor_AA'] = tumor_aa
    res_df = pd.concat([res_df, pd.DataFrame([row_dic])], ignore_index=True)

res_df.to_csv('~/Desktop/Data/pyprojs/Aging/data_subsample/aa_results_clock_rows.csv', index=False, sep=',')


##################################################################################################
cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']



def count_normal_tumor_samples(fn, alias):
    df = pd.read_csv(fn, sep='\t')
    return len(df[df.tissue_type == 'Normal']), len(df[df.tissue_type == 'Tumor']), alias


def rename_columns(df, c_df):
    c_df_dict = c_df.set_index('id').to_dict(orient='index')
    def create_new_col_name(col_name):
        if col_name == 'ProbeID':
            return col_name  # Keep 'ProbeID' unchanged
        if col_name in c_df_dict:
            tissue_type = c_df_dict[col_name]['tissue_type']
            c_age = c_df_dict[col_name]['c_age']
            # Format the c_age with two decimal places and create the new name
            new_col_name = f"{col_name}_{tissue_type[0]}_{c_age:.2f}"
            return new_col_name
        return col_name  # Return original name if no match found
    df.columns = [create_new_col_name(col) for col in df.columns]
    return df


def filter_lownn_cols(df):
    t_columns = df.columns[df.columns.str.contains('_T_')]
    n_columns = df.columns[df.columns.str.contains('_N_')]
    nan_percentage_t = df[t_columns].isna().mean(axis=1)
    nan_percentage_n = df[n_columns].isna().mean(axis=1)
    return df[(nan_percentage_t <= 0.30) & (nan_percentage_n <= 0.30)]

def calculate_p_value(row):
    t_values = row[t_columns].dropna()  # Drop NaNs from '_T_' group
    n_values = row[n_columns].dropna()  # Drop NaNs from '_N_' group
    if len(t_values) > 1 and len(n_values) > 1:  # Ensure there are enough data points
        t_stat, p_value = ttest_ind(t_values, n_values, equal_var=False)
        return p_value
    else:
        return 1  # Assign a high p-value if not enough data points

def mean_difference(row):
    t_mean = row[t_columns].mean()  # Mean of '_T_' columns
    n_mean = row[n_columns].mean()  # Mean of '_N_' columns
    return abs(t_mean - n_mean)  # Absolute difference


def proportional_difference(row):
    t_values = row[t_columns].dropna()  # Drop NaNs from '_T_' group
    n_values = row[n_columns].dropna()  # Drop NaNs from '_N_' group
    n_mean = n_values.mean()  # Mean of '_N_' group
    t_mean = t_values.mean()  # Mean of '_T_' group
    # If T group mean is greater than N group mean, find proportion of T values greater than N mean
    if t_mean > n_mean:
        proportion = (t_values > n_mean).mean()
    # If T group mean is smaller than N group mean, find proportion of T values smaller than N mean
    else:
        proportion = (t_values < n_mean).mean()
    return proportion

suffix = '_pred_chron_result.csv'
#cancer = 'BMnBLD'
for cancer in cancers:
    c_df = pd.read_csv(f'./{cancer}/{cancer}{suffix}', sep='\t')
    df = pd.read_csv(f'{cancer}_methyl_cohort.csv')
    df = rename_columns(df, c_df)
    df = filter_lownn_cols(df)
    pandarallel.initialize(progress_bar=True)
    t_columns = df.columns[df.columns.str.contains('_T_')]
    n_columns = df.columns[df.columns.str.contains('_N_')]
    df['p_value'] = df.parallel_apply(calculate_p_value, axis=1)
    _, fdr_values, _, _ = multipletests(df['p_value'], method='fdr_bh')
    df['fdr'] = fdr_values
    df['mean_diff'] = df.parallel_apply(mean_difference, axis=1)
    df['proportional_diff'] = df.parallel_apply(proportional_difference, axis=1)
    cgs = df[(df.fdr < 0.01) & (df.mean_diff > 0.1) & (df.proportional_diff > 0.8)]['ProbeID']
    np.save(f'./{cancer}/{cancer}_filtered_cgs.npy', np.asarray(cgs.values))

################################################

import numpy as np
cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']

suffix = "_filtered_cgs.npy"
Cs = []
for cancer in cancers:
    Cs.append(np.load(f'./{cancer}/{cancer}{suffix}', allow_pickle=True))

intersection = set(Cs[0]).intersection(*Cs[1:])


####################################################

import os

def find_age_associated_cpgs(df, c_df):
    df = rename_columns(df, c_df)
    df = filter_lownn_cols(df)
    df.index = df['ProbeID']
    n_columns = df.columns[df.columns.str.contains('_N_')]
    n_columns = [list(n_columns)[i] for i in range(len(n_columns))]
    ages = np.array([float(s.split('_')[-1]) for s in n_columns])
    pandarallel.initialize(progress_bar=True, nb_workers=int(os.cpu_count()/2))
    def row_correlation(row):
        mask = ~np.isnan(row) & ~np.isnan(ages)
        return np.corrcoef(row[mask], ages[mask])[0, 1]
    df = df[n_columns]
    df['correlation'] = df.parallel_apply(row_correlation, axis=1)
    df['max_min_diff'] = df.drop(columns=['correlation']).apply(lambda row: row.max() - row.min(), axis=1)
    df[['correlation', 'max_min_diff']].to_csv(f'./{cancer}/{cancer}_cpg_age_correlations.csv')

cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']
for cancer in cancers:
    suffix = '_pred_chron_result.csv'
    c_df = pd.read_csv(f'./{cancer}/{cancer}{suffix}', sep='\t')
    find_age_associated_cpgs(pd.read_csv(f'{cancer}_methyl_cohort.csv'), c_df)

#################################################################################

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']
fig, axes = plt.subplots(2, 4, figsize=(16, 12))  # 4 rows and 2 columns
axes = axes.flatten()  # Flatten the 2D array of axes for easier indexing

for i, cancer in enumerate(cancers):
    df = pd.read_csv(f'/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/age_corr_Nsamples/{cancer}_cpg_age_correlations.csv', index_col=0)
    cpgs = np.load(f'/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/NT_DMCs/{cancer}_filtered_cgs.npy', allow_pickle=True)
    cpgs_df = df.loc[cpgs]
    df = df[df['max_min_diff'] > 0.5]
    cpgs_df = cpgs_df[cpgs_df['max_min_diff'] > 0.5]
    data_to_plot = [df['correlation'].dropna(), cpgs_df['correlation'].dropna()]
    axes[i].violinplot(data_to_plot)
    axes[i].set_ylim([-0.8, 1.0])
    if cancer in ['Lung', 'BMnBLD']:
        axes[i].set_ylabel('mC level correlation with age', fontsize=14)
    else:
        axes[i].set_yticklabels([])  # Remove y-axis labels for other plots
    alias_dic = {'Lung': 'Lung', 'Clorectal': 'Colorectal', 'Breast': 'Breast', 'Thyroid': 'Thyroid', 'BMnBLD': 'Bone Marrow and Blood', 'Kidney': 'Kidney', 'uterus': 'Uterus', 'HeadnNeck': 'Head and Neck'}
    axes[i].set_title(f'{alias_dic[cancer]}', fontsize=14)
    axes[i].axhline(y=0, color='red', linestyle='--')
    axes[i].text(0.1, 0.025, f"n={len(df):,}", transform=axes[i].transAxes,
                 fontsize=14, verticalalignment='bottom', horizontalalignment='left',
                 bbox=dict(facecolor='white', alpha=0.5))
    axes[i].text(0.90, 0.025, f"n={len(cpgs_df):,}", transform=axes[i].transAxes,
                 fontsize=14, verticalalignment='bottom', horizontalalignment='right',
                 bbox=dict(facecolor='white', alpha=0.5))
    if i >= 4:  # Index for the second row
        axes[i].set_xticks([1, 2])
        axes[i].set_xticklabels(['All CpGs', 'Cancer Affected \n CpGs'], fontsize=14)
    else:
        axes[i].set_xticks([])

plt.subplots_adjust(hspace=5)
plt.tight_layout()
plt.savefig('/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/Result/correlation_dist.png')

####################################################################################################


import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

clocks = ['Horvath', 'Hannum', 'Levine', 'BNN', 'skinHorvath', 'BLUP', 'EN']
clock_name = clocks[0]

# for clock_name in clocks
cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']
fig, axes = plt.subplots(2, 4, figsize=(8, 4))  # 4 rows and 2 columns
axes = axes.flatten()  # Flatten the 2D array of axes for easier indexing
is_subsample = True
subsample_alias = "_subsample" if is_subsample else ""
axi=0
clock = 'Horvath'
mins, maxs = [], []
for i, cancer in enumerate(cancers):
    df = pd.read_csv(f'~/Desktop/Data/pyprojs/Aging/data{subsample_alias}/{cancer}_pred_chron_result.csv', sep='\t')
    df['age_diff'] = df[clock_name] - df['c_age']
    df_normal = df[df['tissue_type'] == 'Normal']
    df_tumor = df[df['tissue_type'] == 'Tumor']
    mins.append(min(df_normal['age_diff'].min(), df_tumor['age_diff'].min()))
    maxs.append(max(df_normal['age_diff'].max(), df_tumor['age_diff'].max()))
global_min = min(mins)
global_max = max(maxs)
xticks = np.arange(np.ceil(global_min / 50) * 50, np.ceil(global_max / 50) * 50, 50)
for i, cancer in enumerate(cancers):
    df = pd.read_csv(f'~/Desktop/Data/pyprojs/Aging/data{subsample_alias}/{cancer}_pred_chron_result.csv', sep='\t')
    df['age_diff'] = df[clock_name] - df['c_age']
    df_normal = df[df['tissue_type'] == 'Normal']
    df_tumor = df[df['tissue_type'] == 'Tumor']
    sns.kdeplot(df_normal['age_diff'], label='Normal', fill=True, color='blue', ax=axes[axi])
    sns.kdeplot(df_tumor['age_diff'], label='Tumor', fill=True, color='red', ax=axes[axi])
    alias_dic = {'Lung': 'Lung', 'Clorectal': 'Colorectal', 'Breast': 'Breast', 'Thyroid': 'Thyroid', 'BMnBLD': 'Bone Marrow and Blood', 'Kidney': 'Kidney', 'uterus': 'Uterus', 'HeadnNeck': 'Head and Neck'}
    axes[axi].set_title(f'{alias_dic[cancer]}', fontsize=16)
    axes[axi].set_ylim([0, 0.08])
    axes[axi].set_xlim([-100, 200])
    # axes[axi].set_xticks(xticks)
    axes[axi].tick_params(axis='x', labelsize=8)
    if axi >= 4:  # Index for the second row
        axes[axi].set_xlabel('Age Acceleration', fontsize=16)
    else:
        axes[axi].set_xlabel('')
    axes[axi].tick_params(axis='x', labelsize=16)
    if axi % 4 == 0:
        axes[axi].set_ylabel('Density', fontsize=16)
    else:
        axes[axi].set_ylabel('', fontsize=0)
        axes[axi].set_yticklabels([])
    axes[axi].axvline(x=0, color='red', linestyle='--')
    axi+=1

handles, labels = axes[0].get_legend_handles_labels()
fig.legend(handles, ['Normal', 'Tumor'], loc='upper center', fontsize=16, ncol=2, bbox_to_anchor=(0.5, 0.90))
plt.tight_layout()
plt.gcf().set_size_inches(14, 7)
plt.savefig(f'/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/Result/plots/AA_dist/AA_distributions_{subsample_alias}_Horvath.png')

#####################################################################################

#age distribution plot

import pandas as pd
import matplotlib.pyplot as plt
cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']
is_subsample = True
subsample_alias = "_subsample" if is_subsample else ""
fig, axes = plt.subplots(3, 8, figsize=(16, 8))
axes = axes.flatten()  # Flatten the axes array for easier iteration
ax_i = 0
bins = np.linspace(0, 100, 10)
for group in ['all', 'Tumor', 'Normal']:
    x_min, x_max = float('inf'), float('-inf')
    y_max = float('-inf')
    for cancer in cancers:
        df = pd.read_csv(f'~/Desktop/Data/pyprojs/Aging/data{subsample_alias}/{cancer}_pred_chron_result.csv', sep='\t')
        if group != 'all':
            df = df[df['tissue_type'] == group]
        x_min = min(x_min, df['c_age'].min())
        x_max = max(x_max, df['c_age'].max())
        counts, _ = np.histogram(df['c_age'].dropna(), bins=bins)
        y_max = max(y_max, counts.max())
    for i, cancer in enumerate(cancers):
        df = pd.read_csv(f'~/Desktop/Data/pyprojs/Aging/data{subsample_alias}/{cancer}_pred_chron_result.csv', sep='\t')
        if group != 'all':
            df = df[df['tissue_type'] == group]
        axes[ax_i].hist(df['c_age'].dropna(), bins=bins, color='skyblue', edgecolor='black')
        if ax_i < 8:
            t = cancer
            if t == 'BMnBLD':
                t = 'Bone Marrow \n And Blood'
            if t == 'HeadnNeck':
                t = 'Head And \n Neck'
            if t == 'Clorectal':
                t = 'Colorectal'
            axes[ax_i].set_title(t, fontsize=18)
        else:
            axes[ax_i].set_title(None)
        axes[ax_i].set_xlim(x_min, x_max)
        axes[ax_i].set_ylim(0, 400)
        if ax_i > 15:
            axes[ax_i].set_xlabel('Age', fontsize=20)
            axes[ax_i].set_ylim(0, 100)
        else:
            axes[ax_i].set_xlabel(None)
        if ax_i % 8 == 0:
            axes[ax_i].set_ylabel('Number of\nsamples', fontsize=18)
        else:
            axes[ax_i].set_ylabel(None)
            axes[ax_i].set_yticks([])
        ax_i+=1

plt.tight_layout()
plt.savefig(f'/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/Result/age_distribution{subsample_alias}.png')

############################################################
import matplotlib.pyplot as plt
len_Ts = []
len_Ns = []
cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']
alias_dic = {'Lung': 'Lung', 'Clorectal': 'Colorectal', 'Breast': 'Breast', 'Thyroid': 'Thyroid', 'BMnBLD': 'Bone Marrow and Blood', 'Kidney': 'Kidney', 'uterus': 'Uterus', 'HeadnNeck': 'Head and Neck'}
is_subsample = True
subsample_alias = "_subsample" if is_subsample else ""

for cancer in cancers:
    df = pd.read_csv(f'~/Desktop/Data/pyprojs/Aging/data{subsample_alias}/{cancer}_pred_chron_result.csv', sep='\t')
    len_Ts.append(len(df[df.tissue_type == 'Tumor']))
    len_Ns.append(len(df[df.tissue_type == 'Normal']))

labels = [alias_dic[cancer] for cancer in cancers]

x = np.arange(len(cancers))  # the label locations
width = 0.35  # the width of the bars

fig, ax = plt.subplots(figsize=(14, 7))

# Plot bars for Tumor and Normal sample lengths
bars1 = ax.bar(x - width/2, len_Ts, width, label='Tumor', color='red', edgecolor='black', linewidth=1.2, alpha=0.6)
bars2 = ax.bar(x + width/2, len_Ns, width, label='Normal', color='blue', edgecolor='black', linewidth=1.2, alpha=0.6)

# Add labels, title, and legend
# ax.set_xlabel('Cancer Types', fontsize=14, labelpad=15)
ax.set_ylabel('Number of Samples', fontsize=22, labelpad=15)
# ax.set_title('Number of Tumor and Normal Samples by Cancer Type', fontsize=16, pad=20)
ax.set_xticks(x)
ax.set_xticklabels(labels, fontsize=22, rotation=45, ha='right')
ax.legend(fontsize=22)

# Add values on top of each bar
for bar in bars1 + bars2:
    yval = bar.get_height()
    ax.text(bar.get_x() + bar.get_width() / 2, yval + 2, int(yval), ha='center', va='bottom', fontsize=18, fontweight='bold')

# Remove borders and add gridlines for professionalism
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.yaxis.grid(True, linestyle='--', alpha=0.7)

plt.tight_layout()
plt.savefig(f'/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/Result/sample_number{subsample_alias}.png')


#################################################################################

cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']

for i, cancer in enumerate(cancers):
    df = pd.read_csv(f'/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/age_corr_Nsamples/{cancer}_cpg_age_correlations.csv', index_col=0)
    cpgs = np.load(f'/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/NT_DMCs/{cancer}_filtered_cgs.npy', allow_pickle=True)
    df = df[df['max_min_diff'] > 0.5]
    cc = set(df[(df['correlation'] > 0.7) | (df['correlation'] < -0.7)].index)
    cc_all = set(df.index)
    print(f'{cancer},{len(cc)},{len((cc & set(cpgs)))},{len(df)},{len((cc_all & set(cpgs)))}')
