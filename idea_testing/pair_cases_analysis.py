import pandas as pd

cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']

for cancer in cancers:
    df = pd.read_csv(f'./{cancer}/file_case.txt', sep='\t')
    new_df = df[df.duplicated('case_ids', keep=False)]  # Filter rows with duplicate case_ids
    new_df = new_df.groupby('case_ids').filter(lambda x: len(x) == 2 and set(x['tissue_type']) == {'Tumor', 'Normal'})  # Keep only pairs with one Tumor and one Normal
    result_df = new_df.pivot(index='case_ids', columns='tissue_type', values='file_id').reset_index().rename(columns={'Tumor': 'Tumor_file_id', 'Normal': 'Normal_file_id'})
    result_df = result_df[['case_ids', 'Tumor_file_id', 'Normal_file_id']].dropna().reset_index(drop=True)
    failed_cases = []
    for case in result_df.case_ids.values:
        sub_df = df[df.case_ids == case]
        if len(sub_df) != 2:
            print('ERROR, something other than two cases', cancer)
            failed_cases.append(case)
        if sub_df.iloc[0]['c_age'] != sub_df.iloc[1]['c_age']:
            print('Case age mismatch', cancer)
            failed_cases.append(case)
    for index, row in result_df.iterrows():
        if df[df.file_id == row['Tumor_file_id']].iloc[0]['tissue_type'] != 'Tumor':
            print('FAILED TUMOR', cancer)
            failed_cases.append(row['case_ids'])
        if df[df.file_id == row['Normal_file_id']].iloc[0]['tissue_type'] != 'Normal':
            print('FAILED TUMOR', cancer)
            failed_cases.append(row['case_ids'])
    result_df = result_df[~result_df['case_ids'].isin(failed_cases)]
    pres_df = pd.read_csv(f'./{cancer}/{cancer}_pred_chron_result.csv', sep='\t')
    c_age_dic = dict(zip(pres_df['id'], pres_df['c_age']))
    result_df['c_age'] = result_df.apply(lambda row: c_age_dic[row['Tumor_file_id']], axis=1)
    clocks = ['Horvath', 'Hannum', 'Levine', 'BNN', 'skinHorvath', 'BLUP', 'EN']
    for clock in clocks:
        pred_age_dic = dict(zip(pres_df['id'], pres_df[clock]))
        result_df[f'{clock}_N'] = result_df.apply(lambda row: pred_age_dic[row['Normal_file_id']], axis=1)
        result_df[f'{clock}_T'] = result_df.apply(lambda row: pred_age_dic[row['Tumor_file_id']], axis=1)
    result_df.to_csv(f'./{cancer}/{cancer}_paired_samples.csv', index=False)


###############################################################

#plotting

import matplotlib.pyplot as plt
import numpy as np

root = '/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/paired_samples/'
cancer = 'Thyroid'
cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']
alias_dic = {'Lung': 'Lung', 'Clorectal': 'Colorectal', 'Breast': 'Breast', 'Thyroid': 'Thyroid', 'BMnBLD': 'Bone Marrow and Blood', 'Kidney': 'Kidney', 'uterus': 'Uterus', 'HeadnNeck': 'Head and Neck'}
clocks = ['Horvath', 'Hannum', 'Levine', 'BNN', 'skinHorvath', 'BLUP', 'EN']


fig, axes = plt.subplots(7, 8, figsize=(36, 24))
axes = axes.flatten()
axi = 0
for clock in clocks:
    for i, cancer in enumerate(cancers):
        df = pd.read_csv(f'{root}{cancer}_paired_samples.csv')
        # df['avg_clock_N'] = df[[f'{clock}_N' for clock in clocks]].mean(axis=1)
        # df['avg_clock_T'] = df[[f'{clock}_T' for clock in clocks]].mean(axis=1)
        axes[axi].scatter(df['c_age'], df[f'{clock}_N'], color='blue', label='Normal', alpha=0.7)
        axes[axi].scatter(df['c_age'], df[f'{clock}_T'], color='red', label='Tumor', alpha=0.7)
        # axes[i].scatter(df['c_age'], df['avg_clock_N'], color='blue', label='Normal', alpha=0.7)
        # axes[i].scatter(df['c_age'], df['avg_clock_T'], color='red', label='Tumor', alpha=0.7)
        if axi >= 48:
            axes[axi].set_xlabel('Chronological Age', fontsize=18)
        else:
            axes[axi].set_xlabel(None)
        if axi % 8 == 0:
            #axes[axi].set_ylabel(f'Predicted Age by {clock} Clock')
            axes[axi].set_ylabel(f'Predicted Age',  fontsize=18)
        else:
            axes[i].set_ylabel(None)
        axes[i].set_title(f'{alias_dic[cancer]}', fontsize=18)
        min_val = min(axes[axi].get_xlim()[0], axes[axi].get_ylim()[0])
        max_val = max(axes[axi].get_xlim()[1], axes[axi].get_ylim()[1])
        axes[axi].set_xlim(-5, max_val)
        axes[axi].set_ylim(-5, max_val)
        axes[axi].text(0.05, 0.95, f'n = {len(df)}', transform=axes[axi].transAxes, fontsize=12, verticalalignment='top')
        axes[axi].grid(True)
        axes[axi].plot([0, max_val], [0, max_val], linestyle='--', color='black')
        axi+=1

handles, labels = axes[0].get_legend_handles_labels()
fig.legend(handles, ['Normal', 'Tumor'], loc='upper center', fontsize=18, ncol=2, bbox_to_anchor=(0.5, 0.03))
# plt.subplots_adjust(top=0.95, bottom=0.15)
plt.tight_layout()
plt.subplots_adjust(wspace=0.3, hspace=1, bottom=0.08)  # Increase bottom padding for the legend
plt.gcf().set_size_inches(28, 20)
#plt.savefig(f'/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/Result/plots/paired/chronological_vs_pred_{clock}.png')
plt.savefig(f'/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/Result/plots/paired/chronological_vs_pred_pppp_.png', bbox_inches='tight')
# plt.savefig(f'/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/Result/plots/paired/chronological_vs_pred_averaged.png')


###################################################################################


import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

root = '/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/paired_samples/'
cancer = 'Thyroid'
cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']
alias_dic = {'Lung': 'Lung', 'Clorectal': 'Colorectal', 'Breast': 'Breast', 'Thyroid': 'Thyroid', 'BMnBLD': 'Bone Marrow and Blood', 'Kidney': 'Kidney', 'uterus': 'Uterus', 'HeadnNeck': 'Head and Neck'}
clocks = ['Horvath', 'Hannum', 'Levine', 'BNN', 'skinHorvath', 'BLUP', 'EN']

clock =  'Horvath'
fig, axes = plt.subplots(2, 4, figsize=(32, 12))
axes = axes.flatten()
axi = 0
for i, cancer in enumerate(cancers):
    df = pd.read_csv(f'{root}{cancer}_paired_samples.csv')
    # df['avg_clock_N'] = df[[f'{clock}_N' for clock in clocks]].mean(axis=1)
    # df['avg_clock_T'] = df[[f'{clock}_T' for clock in clocks]].mean(axis=1)
    axes[axi].scatter(df['c_age'], df[f'{clock}_N'], color='blue', label='Normal', alpha=0.7)
    axes[axi].scatter(df['c_age'], df[f'{clock}_T'], color='red', label='Tumor', alpha=0.7)
    # axes[i].scatter(df['c_age'], df['avg_clock_N'], color='blue', label='Normal', alpha=0.7)
    # axes[i].scatter(df['c_age'], df['avg_clock_T'], color='red', label='Tumor', alpha=0.7)
    if axi >= 4:
        axes[axi].set_xlabel('Chronological Age', fontsize=18)
    else:
        axes[axi].set_xlabel(None)
    if axi % 4 == 0:
        #axes[axi].set_ylabel(f'Predicted Age by {clock} Clock')
        axes[axi].set_ylabel(f'Predicted Age',  fontsize=18)
    else:
        axes[i].set_ylabel(None)
    axes[i].set_title(f'{alias_dic[cancer]}', fontsize=18)
    min_val = min(axes[axi].get_xlim()[0], axes[axi].get_ylim()[0])
    max_val = max(axes[axi].get_xlim()[1], axes[axi].get_ylim()[1])
    axes[axi].set_xlim(-5, max_val)
    axes[axi].set_ylim(-5, max_val)
    axes[axi].text(0.05, 0.95, f'n = {len(df)}', transform=axes[axi].transAxes, fontsize=12, verticalalignment='top')
    axes[axi].grid(True)
    axes[axi].plot([0, max_val], [0, max_val], linestyle='--', color='black')
    axi+=1

handles, labels = axes[0].get_legend_handles_labels()
fig.legend(handles, ['Normal', 'Tumor'], loc='upper center', fontsize=18, ncol=2, bbox_to_anchor=(0.5, 0.03))
plt.subplots_adjust(top=0.95, bottom=0.15)
# plt.tight_layout()
plt.gcf().set_size_inches(20, 10)
plt.savefig(f'/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/Result/plots/paired/chronological_vs_pred_{clock}_.png', bbox_inches='tight')
