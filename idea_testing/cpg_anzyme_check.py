import pandas as pd
import configs
from scipy.stats import hypergeom
import numpy as np

# df = pd.read_csv('/home/ssere004/projects/aging/data/humanmethylation450_15017482_v1-2.csv', sep=',', skiprows=7)
df = pd.read_csv('/home/ssere004/projects/aging/data/infinium-methylationepic-v-1-0-b5-manifest-file.csv', sep=',', skiprows=7)
df = df[['Name', 'CHR', 'MAPINFO']]
# cpg_list = pd.read_csv('/home/ssere004/projects/aging/data/rapid_gain_cpgs.txt', header=None, names=['Name'])
# cpg_list = pd.read_csv('/home/ssere004/projects/aging/data/cortex_cpg.txt', header=None, names=['Name'])
cpg_list = pd.read_csv('/home/ssere004/projects/aging/data/blood_epic_cpg.txt', header=None, names=['Name'])
df = cpg_list.merge(df, left_on=['Name'], right_on=['Name'], how='inner')
df.columns = ['Name', 'chr', 'position']
df = df.dropna()
df['position'] = df['position'].astype(int)
df['chr'] = df['chr'].apply(lambda x: 'chr' + str(x))
df['chr'] = df['chr'].apply(lambda x: x.lower())

KO_dmrs = {'TET': configs.TKO_DMRs_address,
           'DNMT3': configs.DKO_DMRs_address,
           'QKO': configs.QKO_DMRs_address,
           'PKO': configs.PKO_DMRs_address}

KO_notdmrs = {'TET': configs.TKO_notDMRs_address,
           'DNMT3': configs.DKO_notDMRs_address,
           'QKO': configs.QKO_notDMRs_address,
           'PKO': configs.PKO_notDMRs_address}

for ko_name in KO_dmrs.keys():
    dmrs = pd.read_csv(KO_dmrs[ko_name], header=None, sep='\t', names=['chr', 'position', 'meth1', 'coverage1', 'meth2', 'coverage2', 'p_value'])
    not_dmrs = pd.read_csv(KO_notdmrs[ko_name], header=None, sep='\t', names=['chr', 'position', 'meth1', 'coverage1', 'meth2', 'coverage2'])
    dmc_c = 0
    ndmc_c = 0
    for index, row in df.iterrows():
        if len(dmrs[(dmrs['chr'] == 'chr' + str(row['CHR'])) & (dmrs['position'] == int(row['MAPINFO']) + 1)]) != 0:
            dmc_c += 1
        if len(not_dmrs[(not_dmrs['chr'] == 'chr' + str(row['CHR'])) & (not_dmrs['position'] == int(row['MAPINFO']) + 1)]) != 0:
            ndmc_c += 1
    M, n, N, K = len(dmrs) + len(not_dmrs), len(dmrs), dmc_c+ndmc_c, dmc_c
    print('num Cs found in {} dmrs: {}, num Cs found in {} notdmrs: {}-----p_value:{}'.format(ko_name, str(dmc_c), ko_name, str(ndmc_c), hypergeom.sf(K-1, M, n, N)))

from Bio import SeqIO
def readfasta(address):
    recs = SeqIO.parse(address, "fasta")
    sequences = {}
    for chro in recs:
        sequences[chro.id.lower()] = chro.seq
    for i in sequences.keys():
        sequences[i] = sequences[i].upper()
    return sequences

assembly = readfasta(configs.hg19['assembly'])

df['bp'] = df.apply(lambda row: assembly[row['chr']][row['position'] - 1], axis=1)
print(len(df[df['bp'] != 'C'])

c = 0
for index, row in dmrs.iterrows():
    print(assembly[str(row['chr'])][int(row['position'])])
    c += 1
    if c == 50:
        break



############################## Check WT and KO methylation levels of Aging CpGs ##################################


df.position = df.position - 1


meth_root = '/home/ssere004/Organisms/ES/WGBS/hues8/'
wt_meth_ad = meth_root + 'GSM3618718_HUES8_WT_WGBS.bed'
KO_meth_adds = {'DKO': meth_root + 'GSM4458672_WGBS_HUES8_DKO_P6.bed',
              'TKO': meth_root + 'GSM3618720_HUES8_TKO_WGBS.bed',
              'QKO': meth_root + 'GSM3618719_HUES8_QKO_WGBS.bed',
              'PKO': meth_root + 'GSM3618721_HUES8_PKO_WGBS.bed',
              }


meth_df_wt = pd.read_csv(wt_meth_ad, sep='\t', names=['chr', 'position', 'end', 'meth', 'coverage'])
for ko in KO_meth_adds.keys():
    if ko != 'DKO':
        meth_df_ko = pd.read_csv(KO_meth_adds[ko], sep='\t', names=['chr', 'position', 'end', 'meth', 'coverage'])
    else:
        meth_df_ko = pd.read_csv(KO_meth_adds[ko], sep='\t').iloc[:, :5]
        meth_df_ko.columns = ['chr', 'position', 'end', 'meth', 'coverage']
    print(ko + " ##########################")
    result = meth_df_ko.merge(df, on=['chr', 'position'])
    print('ko average: ' + str(np.mean(result.meth)) + ' len ko: ' + str(len(result)))
    result = meth_df_wt.merge(df, on=['chr', 'position'])
    print('wt average: ' + str(np.mean(result.meth)) + ' len wt: ' + str(len(result)))


################################
