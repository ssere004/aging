import pandas as pd
import numpy as np
from fractions import Fraction

cancer = 'Kidney'
cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']
for cancer in cancers:
    df = pd.read_csv(f'~/Desktop/Data/pyprojs/Aging/data/{cancer}_pred_chron_result.csv', sep='\t')

    # Assuming 'df' is your original DataFrame

    # 1. Bin the Ages
    min_age = df['c_age'].min()
    max_age = df['c_age'].max()
    bin_edges = np.arange(10 * np.floor(min_age / 10), 10 * np.ceil(max_age / 10) + 10, 10)
    df['age_bin'] = pd.cut(df['c_age'], bins=bin_edges, right=False)

    # 2. Compute counts per bin
    counts = df.groupby(['age_bin', 'tissue_type']).size().unstack(fill_value=0).reset_index()
    counts.columns = ['age_bin', 'Normal', 'Tumor']

    # Exclude bins where 'Normal' samples are zero to avoid division by zero
    counts = counts[counts['Normal'] > 0].copy()

    # 3. Compute Tumor-to-Normal ratio per bin
    counts['ratio'] = counts['Tumor'] / counts['Normal']

    # 4. Find r_min = minimum Tumor-to-Normal ratio across bins
    r_min = counts['ratio'].min()

    # 5. For each bin, calculate the number of samples to include
    def calculate_samples(row):
        n_normal = row['Normal']  # Take all 'Normal' samples
        n_tumor_needed = int(np.floor(r_min * n_normal))
        available_tumor = row['Tumor']

        if n_tumor_needed <= available_tumor:
            n_tumor = n_tumor_needed
        else:
            # Adjust 'Normal' samples to match available 'Tumor' samples while maintaining ratio
            n_tumor = available_tumor
            n_normal = int(np.floor(n_tumor / r_min))
            n_tumor = int(np.floor(r_min * n_normal))

        return pd.Series({'Normal_samples': n_normal, 'Tumor_samples': n_tumor})

    counts[['Normal_samples', 'Tumor_samples']] = counts.apply(calculate_samples, axis=1)

    # 6. Sample the data
    sampled_dfs = []
    for _, row in counts.iterrows():
        bin_data = df[df['age_bin'] == row['age_bin']]
        normal_samples = bin_data[bin_data['tissue_type'] == 'Normal'].sample(
            n=int(row['Normal_samples']), random_state=42)
        tumor_samples = bin_data[bin_data['tissue_type'] == 'Tumor'].sample(
            n=int(row['Tumor_samples']), random_state=42)
        sampled_dfs.extend([normal_samples, tumor_samples])

    # Combine sampled dataframes
    subset_df = pd.concat(sampled_dfs).reset_index(drop=True)
    subset_df.to_csv(f'~/Desktop/Data/pyprojs/Aging/data_subsample/{cancer}_pred_chron_result.csv', index=False, sep='\t')
