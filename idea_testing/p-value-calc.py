
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']
fig, axes = plt.subplots(2, 4, figsize=(16, 12))  # 4 rows and 2 columns
axes = axes.flatten()  # Flatten the 2D array of axes for easier indexing

for i, cancer in enumerate(cancers):
    df = pd.read_csv(f'/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/age_corr_Nsamples/{cancer}_cpg_age_correlations.csv', index_col=0)
    cpgs = np.load(f'/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/NT_DMCs/{cancer}_filtered_cgs.npy', allow_pickle=True)
    cpgs_df = df.loc[cpgs]
    df = df[df['max_min_diff'] > 0.5]
    cpgs_df = cpgs_df[cpgs_df['max_min_diff'] > 0.5]
    data_to_plot = [df['correlation'].dropna(), cpgs_df['correlation'].dropna()]
    axes[i].violinplot(data_to_plot)
    axes[i].set_ylim([-0.8, 1.0])
    if cancer in ['Lung', 'BMnBLD']:
        axes[i].set_ylabel('mC level correlation with age', fontsize=14)
    else:
        axes[i].set_yticklabels([])  # Remove y-axis labels for other plots
    alias_dic = {'Lung': 'Lung', 'Clorectal': 'Colorectal', 'Breast': 'Breast', 'Thyroid': 'Thyroid', 'BMnBLD': 'Bone Marrow and Blood', 'Kidney': 'Kidney', 'uterus': 'Uterus', 'HeadnNeck': 'Head and Neck'}
    axes[i].set_title(f'{alias_dic[cancer]}', fontsize=14)
    axes[i].axhline(y=0, color='red', linestyle='--')
    axes[i].text(0.1, 0.025, f"n={len(df):,}", transform=axes[i].transAxes,
                 fontsize=14, verticalalignment='bottom', horizontalalignment='left',
                 bbox=dict(facecolor='white', alpha=0.5))
    axes[i].text(0.90, 0.025, f"n={len(cpgs_df):,}", transform=axes[i].transAxes,
                 fontsize=14, verticalalignment='bottom', horizontalalignment='right',
                 bbox=dict(facecolor='white', alpha=0.5))
    if i >= 4:  # Index for the second row
        axes[i].set_xticks([1, 2])
        axes[i].set_xticklabels(['All CpGs', 'Cancer Affected \n CpGs'], fontsize=14)
    else:
        axes[i].set_xticks([])

plt.subplots_adjust(hspace=5)
plt.tight_layout()
plt.savefig('/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/Result/correlation_dist.png')




#####################################

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']
fig, axes = plt.subplots(2, 4, figsize=(16, 12))  # 4 rows and 2 columns
axes = axes.flatten()  # Flatten the 2D array of axes for easier indexing

for i, cancer in enumerate(cancers):
    df = pd.read_csv(f'/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/age_corr_Nsamples/{cancer}_cpg_age_correlations.csv', index_col=0)
    cpgs = np.load(f'/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/NT_DMCs/{cancer}_filtered_cgs.npy', allow_pickle=True)
    cpgs_df = df.loc[cpgs]
    df = df[df['max_min_diff'] > 0.5]
    cpgs_df = cpgs_df[cpgs_df['max_min_diff'] > 0.5]
    data_to_plot = [df['correlation'].dropna(), cpgs_df['correlation'].dropna()]
    axes[i].violinplot(data_to_plot)
    axes[i].set_ylim([-0.8, 1.0])
    if cancer in ['Lung', 'BMnBLD']:
        axes[i].set_ylabel('mC level correlation with age', fontsize=14)
    else:
        axes[i].set_yticklabels([])  # Remove y-axis labels for other plots
    alias_dic = {'Lung': 'Lung', 'Clorectal': 'Colorectal', 'Breast': 'Breast', 'Thyroid': 'Thyroid', 'BMnBLD': 'Bone Marrow and Blood', 'Kidney': 'Kidney', 'uterus': 'Uterus', 'HeadnNeck': 'Head and Neck'}
    axes[i].set_title(f'{alias_dic[cancer]}', fontsize=14)
    axes[i].axhline(y=0, color='red', linestyle='--')
    axes[i].text(0.1, 0.025, f"n={len(df):,}", transform=axes[i].transAxes,
                 fontsize=14, verticalalignment='bottom', horizontalalignment='left',
                 bbox=dict(facecolor='white', alpha=0.5))
    axes[i].text(0.90, 0.025, f"n={len(cpgs_df):,}", transform=axes[i].transAxes,
                 fontsize=14, verticalalignment='bottom', horizontalalignment='right',
                 bbox=dict(facecolor='white', alpha=0.5))
    if i >= 4:  # Index for the second row
        axes[i].set_xticks([1, 2])
        axes[i].set_xticklabels(['All CpGs', 'Cancer Affected \n CpGs'], fontsize=14)
    else:
        axes[i].set_xticks([])

plt.subplots_adjust(hspace=5)
plt.tight_layout()
plt.savefig('/Users/salehsereshki/Desktop/Data/UCRRA/projects/aging/Result/correlation_dist.png')
