import pandas as pd
import configs
from scipy.stats import hypergeom
import numpy as np

# df = pd.read_csv('/home/ssere004/projects/aging/data/humanmethylation450_15017482_v1-2.csv', sep=',', skiprows=7)
df = pd.read_csv('/home/ssere004/projects/aging/data/infinium-methylationepic-v-1-0-b5-manifest-file.csv', sep=',', skiprows=7)
df = df[['Name', 'CHR', 'MAPINFO']]
# cpg_list = pd.read_csv('/home/ssere004/projects/aging/data/rapid_gain_cpgs.txt', header=None, names=['Name'])
# cpg_list = pd.read_csv('/home/ssere004/projects/aging/data/cortex_cpg.txt', header=None, names=['Name'])
cpg_list = pd.read_csv('/home/ssere004/projects/aging/data/blood_epic_cpg.txt', header=None, names=['Name'])
df = cpg_list.merge(df, left_on=['Name'], right_on=['Name'], how='inner')
df.columns = ['Name', 'chr', 'position']
df = df.dropna()
df['position'] = df['position'].astype(int)
df['chr'] = df['chr'].apply(lambda x: 'chr' + str(x))
df['chr'] = df['chr'].apply(lambda x: x.lower())


dmrs = df.copy()

df = pd.read_csv('/home/ssere004/projects/aging/data/infinium-methylationepic-v-1-0-b5-manifest-file.csv', sep=',', skiprows=7)
df = df[['Name', 'CHR', 'MAPINFO']]
merged = dmrs.merge(df, on='Name', how='outer', indicator=True)
df = merged[merged['_merge'] == 'right_only'][df.columns]
df.columns = ['Name', 'chr', 'position']
df = df.dropna()
df['position'] = df['position'].astype(int)
df['chr'] = df['chr'].apply(lambda x: 'chr' + str(x))
df['chr'] = df['chr'].apply(lambda x: x.lower())

not_dmrs = df.copy()
