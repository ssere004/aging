import requests
import pandas as pd
import pandas as pd
import os
import numpy as np
from gdc import address_configs as add_cnfg
#this part is for making a dataset which has case_id, sample_id, c_age and DNAm_Age

cnfg = add_cnfg.prostate_add
GDC_folder_path = cnfg['GDC_folder_path']
manifest_add = cnfg['manifest_add']
clinical_add = cnfg['clinical_add']
sample_add = cnfg['sample_add']
horvath_age_output = cnfg['horvath_age_output']
file_case_add = cnfg['file_case_add']
write_output_add = cnfg['write_output_add']

def get_case_ids_from_file_ids(file_ids):
    endpoint = "https://api.gdc.cancer.gov/files"
    params = {
        "filters": {
            "op": "in",
            "content": {
                "field": "file_id",
                "value": file_ids
            }
        },
        "fields": "file_id,cases.case_id,cases.samples.sample_id",
        "format": "JSON",
        "size": "10000"
    }
    response = requests.post(endpoint, json=params)
    response_data = response.json()
    case_ids = []
    sample_ids = []
    for file in response_data["data"]["hits"]:
        for case in file["cases"]:
            case_ids.extend([case["case_id"]])
            sample_ids.extend([sample["sample_id"] for sample in case["samples"]])
    return case_ids, sample_ids


manifest_df = pd.read_csv(manifest_add, sep='\t')
manifest_df.head()

file_ids = []
for index, row in manifest_df.iterrows():
    if 'sesame' in row['filename']:
        file_ids.append(row['id'])

case_ids, sample_ids = get_case_ids_from_file_ids(file_ids)
file_case_df = pd.DataFrame({'file_id': file_ids, 'case_ids': case_ids, 'sample_ids': sample_ids})
file_case_df.to_csv(file_case_add, sep='\t', index=False)


##########################################################

file_case_df = pd.read_csv(file_case_add, sep='\t')
sample_df = pd.read_csv(sample_add, sep='\t')
clinical_df = pd.read_csv(clinical_add, sep='\t')
# res_df = pd.read_csv('/data/saleh/aging/liver/process/fileID_caseID.csv', sep='\t')
#age_df = pd.read_csv('/data/home/ssere004/projects/aging/horvath_methylation_age-master/Output.csv')
age_df = pd.read_csv(horvath_age_output) #This is for the Predicted age
age_df['SampleID'] = age_df['SampleID'].apply(lambda x: x[1:].replace('.', '-') if x.startswith('X') else x.replace('.', '-'))

res_df = pd.merge(file_case_df, age_df[['SampleID', 'DNAmAge']], how='left', left_on='file_id', right_on='SampleID').drop(columns=['SampleID'])
sample_dic = dict(zip(sample_df['sample_id'], sample_df['tissue_type']))
clinical_dic = dict(zip(clinical_df['case_id'], clinical_df['days_to_birth']))
res_df['c_age'] = res_df['case_ids'].apply(lambda x: int(clinical_dic[x])/-365.25)
res_df['tissue_type'] = res_df['sample_ids'].apply(lambda x: sample_dic[x])

######This part is to add tissue_type to file_case_df
file_case_df['tissue_type'] = file_case_df['sample_ids'].apply(lambda x: sample_dic[x])
file_case_df['c_age'] = file_case_df['case_ids'].apply(lambda x: int(clinical_dic[x])/-365.25)
file_case_df.to_csv(file_case_add, sep='\t', index=False)
# THIS IS A TEST UNIT
# c_age = []
# tt = []
# for i in range(200):
#     row = res_df.iloc[i]
#     sm_t = sample_df[sample_df['sample_id'] == row['sample_ids']]
#     cl_t = clinical_df[clinical_df['case_id'] == row['case_ids']]
#     if len(cl_t) != 1:
#         ag = cl_t.iloc[0]['days_to_birth']
#         for index, row in cl_t.iterrows():
#             if row['days_to_birth'] != ag:
#                 print(i, 'ag')
#     if len(sm_t) != 1:
#         print(i, 'sm')
#     c_age.append(int(cl_t.iloc[0]['days_to_birth'])/-365.25)
#     tt.append(sm_t.iloc[0]['tissue_type'])
# c_age == list(res_df.iloc[:200]['c_age'])
# tt == list(res_df.iloc[:200]['tissue_type'])



duplicate_case_ids = res_df['case_ids'].value_counts()[res_df['case_ids'].value_counts() > 1].index.tolist()
two_case_ids = res_df[res_df['case_ids'].isin(duplicate_case_ids)].sort_values(by='case_ids')

res_df[res_df['tissue_type'] == 'Normal'][['case_ids','sample_ids','DNAmAge','c_age', 'tissue_type']].to_csv(write_output_add + '/normal_age_compare.csv', index=False)
res_df[res_df['tissue_type'] == 'Tumor'][['case_ids','sample_ids','DNAmAge','c_age', 'tissue_type']].to_csv(write_output_add + '/tumor_age_compare.csv', index=False)

two_case_ids.to_csv(write_output_add + '/age_compare_cases_with_two_samples.csv', index=False)

younger_tumor = res_df[(res_df['tissue_type'] == 'Tumor') & (res_df['c_age'] < res_df['DNAmAge'])]
older_tumor = res_df[(res_df['tissue_type'] == 'Tumor') & (res_df['c_age'] >= res_df['DNAmAge'])]

younger_tumor.to_csv(write_output_add + '/younger_tumor.csv', index=False)
older_tumor.to_csv(write_output_add + '/older_tumor.csv', index=False)





#################

#This part is for cleaning the sample and clinical dataframes
sample_subset = sample_df[sample_df['sample_id'].isin(res_df['sample_ids'])]
clinical_df = clinical_df.loc[:, ~(clinical_df.apply(lambda col: (col == '\'--').all()))]
clinical_subset = clinical_df[clinical_df['case_id'].isin(res_df['case_ids'])].groupby(['case_id']).first().reset_index()


sample_subset = pd.merge(res_df[['case_ids', 'sample_ids', 'DNAmAge', 'c_age']], sample_subset, how='left', left_on='sample_ids', right_on='sample_id').drop(columns=['sample_id'])
clinical_subset = pd.merge(res_df[['case_ids', 'sample_ids', 'DNAmAge', 'c_age']], clinical_subset, how='left', left_on='case_ids', right_on='case_id').drop(columns=['case_id'])

final_case_df = pd.merge(clinical_subset, sample_subset, how='left', on='sample_ids', suffixes=('', '_duplicate'))
final_case_df = final_case_df.loc[:, ~final_case_df.columns.str.endswith('_duplicate')]


sample_subset.to_csv('sample_subset.csv', index=False)
clinical_subset.to_csv('clinical_subset.csv', index=False)
final_case_df.to_csv('final_case_df.csv', index=False)











diff_days_group = clinical_df.groupby('case_id').filter(lambda x: x['days_to_birth'].nunique() > 1)
if len(diff_days_group) > 0: print('ERROR THERE IS A CASE WITH TWO DIFFERENT days_to_birth, RESOLVE THIS ISSUE AND THEN CONTINUE')


clinical_df = clinical_df[['case_id', 'days_to_birth']].drop_duplicates(subset='case_id')
clinical_df = clean_clinical_df(clinical_df) #This function is written at this page. There are several columns in clinical_df that can have the age info. This column gather the info and make the last decision for the age.
merged_df = pd.merge(file_case_df, clinical_df, how='left', left_on='case_ids', right_on='case_id')
merged_df.drop(columns=['case_id'], inplace=True)

sample_df = sample_df[['sample_id', 'tissue_type']]
merged_df = pd.merge(merged_df, sample_df, how='left', left_on='sample_ids', right_on='sample_id')
merged_df.drop(columns=['sample_id'], inplace=True)

final_df = pd.merge(age_df[['SampleID', 'DNAmAge']], merged_df, how='left', right_on='case_ids', left_on='SampleID')
final_df['DNAmAge'] = pd.to_numeric(final_df['DNAmAge'], errors='coerce')
final_df['days_to_birth'] = pd.to_numeric(final_df['days_to_birth'], errors='coerce')
final_df['DNAmAge'] = final_df['DNAmAge']*365.25
final_df['days_to_birth'] = final_df['days_to_birth']*-1
final_df = final_df.dropna()

final_df[final_df['tissue_type'] == 'Normal'][['days_to_birth', 'DNAmAge']].to_csv(write_output_add + '/normal_age_compare.csv', index=False)
final_df[final_df['tissue_type'] == 'Tumor'][['days_to_birth', 'DNAmAge']].to_csv(write_output_add + '/tumor_age_compare.csv', index=False)








###### This part is for loading the beta values and make a big dataset of CGs and their corresponding beta values in all samples
methylation_dataset = pd.read_csv('/data/saleh/aging/methylation450/humanmethylation450_15017482_v1-2.csv', sep=',', skiprows=7)
methylation_dataset = pd.read_csv(r'C:\Users\ssereshki\Desktop\Saleh\Data\humanmethylation450_15017482_v1-2.csv', sep=',', skiprows=7)

res_df = pd.DataFrame(index=methylation_dataset['IlmnID'])

for index, row in file_case_df.iterrows():
    # folder_path = GDC_folder_path + "\\" +  row['file_id'] + '\\'
    folder_path = GDC_folder_path + "/" +  row['file_id'] + '/'
    files = [f for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path, f))]
    for fnn in files:
        if 'sesame.level3betas' in fnn and os.path.exists(folder_path+fnn):
            case_df = pd.read_csv(folder_path+fnn, sep='\t', names=['cgid', 'mlevel'])
            res_df[row['file_id']] = pd.Series(case_df['mlevel'].values, index=case_df['cgid'])

res_df.to_csv('{}_methyl_cohort.csv'.format(cnfg['alias']), index_label='ProbeID', na_rep='NA')


##################################

res_df_6 = res_df.loc[['cg07503593', 'cg17493885', 'cg22301418', 'cg09772382', 'cg00718470', 'cg04612488']]
t = res_df_6.T
sample_type_dic = dict(zip(file_case_df['file_id'], file_case_df['tissue_type']))
t['sample_type'] = t.index.map(sample_type_dic)
t.to_csv('{}_6cpg.csv'.format(cnfg['alias']))






case_df = pd.read_csv('/data/saleh/aging/liver/new_GDC/0247d61f-c677-4343-9487-149be902f141/8c93d83b-3b52-4b6c-9692-9c6da9a7a3dc.methylation_array.sesame.level3betas.txt', sep='\t', names=['cgid', 'mlevel'])

total_elements = res_df.size

# Count the number of NaN values
nan_count = res_df.isna().sum().sum()

# Calculate the percentage of NaN values
nan_percentage = (nan_count / total_elements) * 100

hc = pd.read_csv('/data/saleh/aging/liver/process/horvath_clock_coeffs.csv', skiprows=2)
horvath_coefficients = hc.set_index('CpGmarker')['CoefficientTraining'].to_dict()

predicted_ages = {}
for column in res_df.columns:
    age = 0
    for cgid in horvath_coefficients.keys():
        if 'Intercept' not in cgid and not pd.isna(res_df.loc[cgid][column]):
            age += horvath_coefficients[cgid] * res_df.loc[cgid][column]
    predicted_ages[column] = age
    print(column, age)



age = 0
count = 0
for cgid in horvath_coefficients.keys():
    if 'Intercept' not in cgid and not pd.isna(res_df.loc[cgid][column]):
        print(horvath_coefficients[cgid], res_df.loc[cgid][column])
        age += horvath_coefficients[cgid] * res_df.loc[cgid][column]
        count+=1
print(age, count)



###############################################################




import pandas as pd
import numpy as np


#There are several columns in clinical_df that can have the age info. This column gather the info and make the last decision for the age.
def clean_clinical_df(clinical_df):
    clinical_df.replace('\'--', np.nan, inplace=True)
    clinical_df['age_at_index'] = pd.to_numeric(clinical_df['age_at_index'], errors='coerce')
    clinical_df['age_at_diagnosis'] = pd.to_numeric(clinical_df['age_at_diagnosis'], errors='coerce')
    clinical_df['days_to_birth'] = pd.to_numeric(clinical_df['days_to_birth'], errors='coerce')
    def get_age_in_days(value, column_name):
        if pd.isnull(value):
            return np.nan
        if column_name == 'days_to_birth':
            # 'days_to_birth' is negative in your data, so take the absolute value
            age_in_days = -value
        else:
            if value < 120:
                # Value is in years, convert to days
                age_in_days = value * 365
            else:
                # Value is already in days
                age_in_days = value
        return age_in_days
    clinical_df['age_at_index_in_days'] = clinical_df.apply(lambda row: get_age_in_days(row['age_at_index'], 'age_at_index'), axis=1)
    clinical_df['age_at_diagnosis_in_days'] = clinical_df.apply(lambda row: get_age_in_days(row['age_at_diagnosis'], 'age_at_diagnosis'), axis=1)
    clinical_df['days_to_birth_in_days'] = clinical_df.apply(lambda row: get_age_in_days(row['days_to_birth'], 'days_to_birth'), axis=1)
    def check_consistency(row):
        ages = []
        if not pd.isnull(row['age_at_index_in_days']):
            ages.append(row['age_at_index_in_days'])
        if not pd.isnull(row['age_at_diagnosis_in_days']):
            ages.append(row['age_at_diagnosis_in_days'])
        if not pd.isnull(row['days_to_birth_in_days']):
            ages.append(row['days_to_birth_in_days'])
        if len(ages) == 0:
            # No data available
            return np.nan, np.nan
        elif len(ages) == 1:
            # Only one age available, cannot check consistency
            return np.nan, ages[0]
        else:
            # Check if all ages are within 365 days of each other
            if max(ages) - min(ages) <= 365 * 1.5:
                # Ages are consistent
                return True, int(np.max(ages))
            else:
                # Ages are inconsistent
                return False, np.nan
    clinical_df[['consistent', 'age_in_days']] = clinical_df.apply(lambda row: pd.Series(check_consistency(row)), axis=1)
    return clinical_df[['case_id', 'age_in_days']]


cancers = ['Lung', 'Clorectal', 'Breast', 'Thyroid', 'BMnBLD', 'Kidney', 'uterus', 'HeadnNeck']

for cancer in cancers:
    clinical_df = pd.read_csv(f'./{cancer}/clinical.tsv', sep='\t')
    clinical_df = clean_clinical_df(clinical_df)
    file_case_df = pd.read_csv(f'./{cancer}/file_case.txt', sep='\t')
    clinical_dic = dict(zip(clinical_df['case_id'], clinical_df['age_in_days']))
    file_case_df['c_age'] = file_case_df['case_ids'].apply(lambda x: int(clinical_dic[x]) / 365.25 if pd.notna(clinical_dic[x]) else np.nan)
    file_case_df.to_csv(f'./{cancer}/file_case.txt', sep='\t', index=False)

for cancer in cancers:
    adf = pd.read_csv(f'./{cancer}/{cancer}_estimates.csv')
    file_case_df = pd.read_csv(f'./{cancer}/file_case.txt', sep='\t')
    merged_df = file_case_df.merge(adf, left_on='file_id', right_on='id')
    merged_df[['id', 'tissue_type', 'c_age', 'Horvath', 'Hannum', 'Levine', 'BNN', 'skinHorvath', 'PedBE', 'Wu', 'TL', 'BLUP', 'EN']].to_csv(f'./{cancer}/{cancer}_pred_chron_result.csv', sep='\t', index=False)


###############################################################

clocks = ['Horvath', 'Hannum', 'Levine', 'BNN', 'skinHorvath', 'PedBE', 'Wu', 'TL', 'BLUP', 'EN']

res_df = pd.DataFrame(columns=['tissue_type', 'Horvath_normal_AA', 'Horvath_tumor_AA',
                               'Hannum_normal_AA','Hannum_tumor_AA',
                               'Levine_normal_AA','Levine_tumor_AA',
                               'BNN_normal_AA', 'BNN_tumor_AA',
                               'skinHorvath_normal_AA', 'skinHorvath_tumor_AA',
                               'PedBE_normal_AA', 'PedBE_tumor_AA',
                               'Wu_normal_AA', 'Wu_tumor_AA',
                               'TL_normal_AA', 'TL_tumor_AA',
                               'BLUP_normal_AA', 'BLUP_tumor_AA',
                               'EN_normal_AA', 'EN_tumor_AA'])
for cancer in cancers:
    ldf = pd.read_csv(f'./{cancer}/{cancer}_pred_chron_result.csv', sep='\t')
    tldf = ldf[ldf.tissue_type == 'Tumor']
    nldf = ldf[ldf.tissue_type == 'Normal']
    r = [cancer]
    for clock in clocks:
        n_aa = np.mean(nldf[clock] - nldf['c_age'])
        t_aa = np.mean(tldf[clock] - tldf['c_age'])
        r.append(n_aa)
        r.append(t_aa)
    res_df.loc[len(res_df)] = r
