import pandas as pd
import sys
import requests
import os
import numpy as np


GDC_folder_path = '/data/saleh/aging/metadata/GDC-meta/' + sys.argv[1] + '/'
manifest_add = GDC_folder_path + 'gdc_manifest.txt'
clinical_add = GDC_folder_path + 'clinical.tsv'
sample_add = GDC_folder_path + 'sample.tsv'
file_case_add = GDC_folder_path + 'file_case.txt'
alias = sys.argv[1]



def get_case_ids_from_file_ids(file_ids):
    endpoint = "https://api.gdc.cancer.gov/files"
    params = {
        "filters": {
            "op": "in",
            "content": {
                "field": "file_id",
                "value": file_ids
            }
        },
        "fields": "file_id,cases.case_id,cases.samples.sample_id",
        "format": "JSON",
        "size": "10000"
    }
    response = requests.post(endpoint, json=params)
    response_data = response.json()
    case_ids = []
    sample_ids = []
    for file in response_data["data"]["hits"]:
        for case in file["cases"]:
            case_ids.extend([case["case_id"]])
            sample_ids.extend([sample["sample_id"] for sample in case["samples"]])
    return case_ids, sample_ids


manifest_df = pd.read_csv(manifest_add, sep='\t')
manifest_df.head()

file_ids = []
for index, row in manifest_df.iterrows():
    if 'sesame' in row['filename']:
        file_ids.append(row['id'])

case_ids, sample_ids = get_case_ids_from_file_ids(file_ids)
file_case_df = pd.DataFrame({'file_id': file_ids, 'case_ids': case_ids, 'sample_ids': sample_ids})
file_case_df.to_csv(file_case_add, sep='\t', index=False)



methylation_dataset = pd.read_csv('/data/saleh/aging/methylation450/humanmethylation450_15017482_v1-2.csv', sep=',', skiprows=7)

sample_df = pd.read_csv(sample_add, sep='\t')
clinical_df = pd.read_csv(clinical_add, sep='\t')
sample_dic = dict(zip(sample_df['sample_id'], sample_df['tissue_type']))
clinical_dic = dict(zip(clinical_df['case_id'], clinical_df['days_to_birth']))
file_case_df['tissue_type'] = file_case_df['sample_ids'].apply(lambda x: sample_dic[x])
file_case_df.to_csv(file_case_add, sep='\t', index=False)

print('file_case_df saved')

print('START MAKING THE METHYLATION DATAFRAME>>>>')

res_df = pd.DataFrame(index=methylation_dataset['IlmnID'])

for index, row in file_case_df.iterrows():
    # folder_path = GDC_folder_path + "\\" +  row['file_id'] + '\\'
    folder_path = GDC_folder_path +  row['file_id'] + '/'
    files = [f for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path, f))]
    for fnn in files:
        if 'sesame.level3betas' in fnn and os.path.exists(folder_path+fnn):
            case_df = pd.read_csv(folder_path+fnn, sep='\t', names=['cgid', 'mlevel'])
            res_df[row['file_id']] = pd.Series(case_df['mlevel'].values, index=case_df['cgid'])

res_df.to_csv('{}_methyl_cohort.csv'.format(alias), index_label='ProbeID', na_rep='NA')


print('DONE')
