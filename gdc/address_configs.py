

thyroid_add = {
    'alias': " thyroid",
    'GDC_folder_path':'/data/saleh/aging/tyroid/GDC/',
    'manifest_add': '/data/saleh/aging/tyroid/GDC/manifest.txt',
    'clinical_add': '/data/saleh/aging/tyroid/process/meta_data/clinical.tsv',
    'sample_add': '/data/saleh/aging/tyroid/process/meta_data/sample.tsv',
    'horvath_age_output': '/data/saleh/aging/tyroid/process2/Output_thyroid.csv',
    'file_case_add': '/data/saleh/aging/tyroid/process2/file_case_df.csv',
    'write_output_add': '/data/saleh/aging/tyroid/process2/',
}


liver_add = {
    'alias': " liver",
    'GDC_folder_path':'/data/saleh/aging/liver/new_GDC/',
    'manifest_add': '/data/saleh/aging/liver/new_GDC/manifest.txt',
    'clinical_add': '/data/saleh/aging/liver/process2/meta_data/clinical.tsv',
    'sample_add': '/data/saleh/aging/liver/process2/meta_data/sample.tsv',
    'horvath_age_output': '/data/saleh/aging/liver/process2/Output_thyroid.csv',
    'file_case_add': '/data/saleh/aging/liver/process2/file_case_df.csv',
    'write_output_add': '/data/saleh/aging/liver/process2/',
}

breast_add = {
    'alias': "breast",
    'GDC_folder_path': r'C:\Users\ssereshki\Desktop\Saleh\Data\GDC-breast\data',
    'manifest_add': r'C:\Users\ssereshki\Desktop\Saleh\Data\GDC-breast\data\gdc_manifest.2024-07-10.txt',
    'clinical_add': r'C:\Users\ssereshki\Desktop\Saleh\Data\GDC-breast\metadata\clinical\clinical.tsv',
    'sample_add': r'C:\Users\ssereshki\Desktop\Saleh\Data\GDC-breast\metadata\biospecimen\sample.tsv',
    'horvath_age_output': '',
    'file_case_add': r'C:\Users\ssereshki\Desktop\Saleh\Data\GDC-breast\metadata\file_case_df.csv',
    'write_output_add': r'C:\Users\ssereshki\Desktop\Saleh\Data\GDC-breast\process\\',
}

lung_add = {
    'alias': " lung",
    'GDC_folder_path': r'C:\Users\ssereshki\Desktop\Saleh\Data\GDC-lung\data',
    'manifest_add': r'C:\Users\ssereshki\Desktop\Saleh\Data\GDC-lung\data\gdc_manifest.2024-07-11.txt',
    'clinical_add': r'C:\Users\ssereshki\Desktop\Saleh\Data\GDC-lung\metadata\clinical\clinical.tsv',
    'sample_add': r'C:\Users\ssereshki\Desktop\Saleh\Data\GDC-lung\metadata\biospecimen\sample.tsv',
    'horvath_age_output': '',
    'file_case_add': r'C:\Users\ssereshki\Desktop\Saleh\Data\GDC-lung\metadata\file_case_df.csv',
    'write_output_add': r'C:\Users\ssereshki\Desktop\Saleh\Data\GDC-lung\process\\',
}




bmnbld_add = {
    'alias': " bmnbld",
    'GDC_folder_path': '/data/saleh/aging/GDC-BMnBLD/data',
    'manifest_add': '/data/saleh/aging/GDC-BMnBLD/data/gdc_manifest.2024-07-14.txt',
    'clinical_add': '/data/saleh/aging/GDC-BMnBLD/metadata/clinical/clinical.tsv',
    'sample_add': '/data/saleh/aging/GDC-BMnBLD/metadata/biospecimen/sample.tsv',
    'horvath_age_output': '',
    'file_case_add': '/data/saleh/aging/GDC-BMnBLD/process/file_case_df.csv',
    'write_output_add': '/data/saleh/aging/GDC-BMnBLD/process',
}


colon_add = {
    'alias': " colon",
    'GDC_folder_path': '/data/saleh/aging/GDC-clorectal/data',
    'manifest_add': '/data/saleh/aging/GDC-clorectal/data/gdc_manifest.colon.txt',
    'clinical_add': '/data/saleh/aging/GDC-clorectal/metadata/clinical/clinical.tsv',
    'sample_add': '/data/saleh/aging/GDC-clorectal/metadata/biospecimen/sample.tsv',
    'horvath_age_output': '',
    'file_case_add': '/data/saleh/aging/GDC-clorectal/process/file_case_df.csv',
    'write_output_add': '/data/saleh/aging/GDC-clorectal/process',
}

prostate_add = {
    'alias': " prostate",
    'GDC_folder_path': '/data/saleh/aging/GDC-prostate/data',
    'manifest_add': '/data/saleh/aging/GDC-prostate/data/gdc_manifest.prostate.txt',
    'clinical_add': '/data/saleh/aging/GDC-prostate/metadata/clinical/clinical.tsv',
    'sample_add': '/data/saleh/aging/GDC-prostate/metadata/biospecimen/sample.tsv',
    'horvath_age_output': '',
    'file_case_add': '/data/saleh/aging/GDC-prostate/process/file_case_df.csv',
    'write_output_add': '/data/saleh/aging/GDC-prostate/process',
}


