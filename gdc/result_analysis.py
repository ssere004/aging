import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# List of dataset addresses
data_adds = ['../data/Lung_pred_chron_result.csv', '../data/Clorectal_pred_chron_result.csv',
             '../data/Breast_pred_chron_result.csv', '../data/Thyroid_pred_chron_result.csv',
             '../data/BMnBLD_pred_chron_result.csv', '../data/Kidney_pred_chron_result.csv',
             '../data/uterus_pred_chron_result.csv', '../data/HeadnNeck_pred_chron_result.csv']

# Initialize plot
fig, axes = plt.subplots(nrows=4, ncols=2, figsize=(15, 20))
axes = axes.flatten()

# Loop over each dataset
for idx, data_path in enumerate(data_adds):
    # Load the dataset
    df = pd.read_csv(data_path[0:], sep='\t')

    # Calculate age acceleration
    df['age_acceleration'] = df['Horvath'] - df['c_age']

    # Separate tumor and normal samples
    tumor_data = df[df['tissue_type'] == 'Tumor']['age_acceleration']
    normal_data = df[df['tissue_type'] == 'Normal']['age_acceleration']

    # Plot kernel density estimate or histograms side by side for tumor and normal
    sns.kdeplot(tumor_data.dropna(), label='Tumor', ax=axes[idx], shade=True, color='r')
    sns.kdeplot(normal_data.dropna(), label='Normal', ax=axes[idx], shade=True, color='b')

    # Set titles and labels
    axes[idx].set_title(f"Age Acceleration: {data_path.split('/')[-1].split('_')[0]} Cancer")
    axes[idx].set_xlabel('Age Acceleration')
    axes[idx].legend()

# Adjust layout for better visibility
plt.tight_layout()

# Show the plot
plt.show()
