import pandas as pd
import configs

from Bio import SeqIO
def readfasta(address):
    recs = SeqIO.parse(address, "fasta")
    sequences = {}
    for chro in recs:
        sequences[chro.id.lower()] = chro.seq
    for i in sequences.keys():
        sequences[i] = sequences[i].upper()
    return sequences

betas_add = '/data/saleh/aging/tyroid/Normal_B_PTC.filter10.70pct.csv'

assembly = readfasta('/data/home/ssere004/Organisms/homo_sapien/assembly/hg19.fa')
df = pd.read_csv('cg_sites.csv')
df['bp'] = df.apply(lambda row: assembly[row['chr'].lower()][row['start']:row['start']], axis=1)

# >>> len(df[(df.bp != 'CG') & (df.bp != 'GC') ])
# 0
# >>> len(df[(df.bp != 'C')])
# 626281
# >>> len(df[(df.bp != 'G')])


methylation_dataset = pd.read_csv('/data/saleh/aging/methylation450/humanmethylation450_15017482_v1-2.csv', sep=',', skiprows=7)
methylation_dataset = methylation_dataset[['IlmnID', 'CHR', 'MAPINFO']]
methylation_dataset = methylation_dataset.dropna()
methylation_dataset['chr'] = methylation_dataset['CHR'].apply(lambda x: f"chr{x}".lower())
methylation_dataset['chr'] = methylation_dataset['chr'].replace('chr22.0', 'chr22')
methylation_dataset['chr'] = methylation_dataset['chr'].replace('chr21.0', 'chr21')
methylation_dataset['MAPINFO'] = methylation_dataset['MAPINFO'] - 1

#methylation_dataset['bp'] = methylation_dataset.apply(lambda row: assembly[row['chr'].lower()][int(row['MAPINFO']-1)], axis=1)

merged_df = pd.merge(df, methylation_dataset, how='left', left_on=['chr', 'start'], right_on=['chr', 'MAPINFO'])
subset_df = merged_df[merged_df['IlmnID'].notna()]

subset_df['bp'] = subset_df.apply(lambda row: assembly[row['chr'].lower()][int(row['start'])], axis=1)
